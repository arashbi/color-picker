// Copyright (c) 2017, Arash Bijan. All rights reserved. Use of this source code
// is governed by a BSD-style license that can be found in the LICENSE file.

import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

const colors = const <Color>[
const Color.fromARGB(255, 255, 0, 0),
const Color.fromARGB(255, 255, 255, 0),
const Color.fromARGB(255, 0, 255, 0),
const Color.fromARGB(255, 0, 255, 255),
const Color.fromARGB(255, 0, 0, 255),
const Color.fromARGB(255, 255, 0, 255),
const Color.fromARGB(255, 255, 0, 0),
];

const stops = const [0.0, 0.16, 0.33, 0.5, 0.664, 0.833, 1.0];

class ColorPicker extends StatefulWidget {
  double initialValue;
  ValueChanged<Color> onChange;

  ColorPicker(this.initialValue, this.onChange);

  @override
  State createState() {
    return new _ColorPickerState(initialValue, onChange);
  }
}

class _ColorPickerState extends State<ColorPicker> {
  double _value;
  ValueChanged<Color> _onChange;


  _ColorPickerState(this._value, this._onChange);

  @override
  Widget build(BuildContext context) {
    return new Container(

        child: new Container(
            height: 40.0,
            child: new Column(

                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: [

                  new Expanded(

                      child: new Padding(
                        padding: const EdgeInsets.only(left:15.0, right:15.0),
                        child: new DecoratedBox(
                          decoration: new BoxDecoration(
                            gradient: new LinearGradient(
                                colors: colors,
                                stops:  stops,
                            ),


                          )
                      )
                      )
                  ),
                  new Slider(
                      value: _value,
                      min: 0.0,
                      max: 1.0,
                      onChanged: (value) {
                        setState(() {
                          _value = value;
                        });
                        this._onChange(findColor());
                      }
                  )
                ]
            )
        )
    );
  }

  Color findColor(){
    int from,to;
    for(int i=0; i < 7; i++){
      if (stops[i]> _value){
        from = i-1;
        to = i;
        break;
      }
    }
    Color fromColor= colors[from];
    Color toColor = colors[to];
    double  value1 = (_value - stops[from])/(stops[to] - stops[from]);
    int value = (value1 * 255).floor();
    if(fromColor.red != toColor.red){
      return new Color.fromARGB(255, (value - fromColor.red).abs(), fromColor.green, fromColor.blue);
    }
    if(fromColor.green != toColor.green){
      return new Color.fromARGB(255, fromColor.red, (value - fromColor.green).abs(), fromColor.blue);
    }
    if(fromColor.blue != toColor.blue ){
      return new Color.fromARGB(255, fromColor.red, fromColor.green, (value - fromColor.blue).abs());
    }
  }
}
