// Copyright (c) 2017, Arash Bijan. All rights reserved. Use of this source code
// is governed by a BSD-style license that can be found in the LICENSE file.

import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

const List<Color> _colors = const <Color>[
  const Color.fromARGB(255, 255, 0, 0),//0
  const Color.fromARGB(255, 255, 255, 0),//1
  const Color.fromARGB(255, 0, 255, 0),//2
  const Color.fromARGB(255, 0, 255, 255),//3
  const Color.fromARGB(255, 0, 0, 255),//4
  const Color.fromARGB(255, 255, 0, 255),//5
  const Color.fromARGB(255, 255, 0, 0),//6
];

const stops = const [0.0, 0.16, 0.33, 0.5, 0.664, 0.833, 1.0];
double _findValue(Color initialColor) {
  var r = initialColor.red;
  var g = initialColor.green;
  var b = initialColor.blue;
  if( r == 255 && b == 0 ){
    return (stops[1] - stops[0])* g /255;
  }

  if( g == 255  && b == 0 ){
    return (stops[2] - stops[1])* (255- r) /255 + stops[1];
  }

  if( r == 0 && g == 255 ){
    return (stops[3] - stops[2])* b /255 + stops[2];
  }

  if( r == 0 && b == 255 ){
    return (stops[4] - stops[3])* (255 - g) /255 +  stops[3];
  }

  if( g == 0 && b == 255 ){
    return (stops[5] - stops[4])* r /255 +  stops[4];
  }

  if( r == 255 && g == 0 ){
    return (stops[6] - stops[5])* (255 - b) /255 + stops[5];
  }
}
class ColorPicker extends StatefulWidget {
  Color initialColor;
  ValueChanged<Color> onChange;

  ColorPicker( this.initialColor, this.onChange);

  @override
  State createState() {
    return new _ColorPickerState( onChange, initialColor);
  }
}

class _ColorPickerState extends State<ColorPicker> {
  double _value;
  Color _initialColor;
  ValueChanged<Color> _onChange;


  _ColorPickerState( this._onChange, this._initialColor){
    _value = _findValue(_initialColor);
  }

  @override
  Widget build(BuildContext context) {
    return new Container(

        child: new Container(
            height: 40.0,
            child: new Column(

                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: [

                  new Expanded(

                      child: new Padding(
                          padding: const EdgeInsets.only(left:15.0, right:15.0),
                          child: new DecoratedBox(
                              decoration: new BoxDecoration(
                                gradient: new LinearGradient(
                                  colors: _colors,
                                  stops:  stops,
                                ),


                              )
                          )
                      )
                  ),
                  new Slider(
                      value: _value,
                      min: 0.0,
                      max: 1.0,
                      onChanged: (value) {
                        setState(() {
                          _value = value;
                        });
                        this._onChange(findColor());
                      }
                  )
                ]
            )
        )
    );
  }

  Color findColor(){
    int from,to;
    for(int i=0; i < 7; i++){
      if (stops[i]> _value){
        from = i-1;
        to = i;
        break;
      }
    }
    Color fromColor= _colors[from];
    Color toColor = _colors[to];
    double  value1 = (_value - stops[from])/(stops[to] - stops[from]);
    int value = (value1 * 255).floor();
    if(fromColor.red != toColor.red){
      return new Color.fromARGB(255, (value - fromColor.red).abs(), fromColor.green, fromColor.blue);
    }
    if(fromColor.green != toColor.green){
      return new Color.fromARGB(255, fromColor.red, (value - fromColor.green).abs(), fromColor.blue);
    }
    if(fromColor.blue != toColor.blue ){
      return new Color.fromARGB(255, fromColor.red, fromColor.green, (value - fromColor.blue).abs());
    }
  }


}
