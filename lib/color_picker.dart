// Copyright (c) 2017, Arash Bijan. All rights reserved. Use of this source code
// is governed by a BSD-style license that can be found in the LICENSE file.

/// Support for doing something awesome.
///
/// More dartdocs go here.
library color_picker;

export 'src/color_picker_base.dart';

// TODO: Export any libraries intended for clients of this package.
